from rest_framework import serializers
from marketApp.models.user import User
from marketApp.models.sale import Sale
from marketApp.serializers.saleSerializer import SaleSerializer


class UserSerializer(serializers.ModelSerializer):
    sale = SaleSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'name', 'email', 'password', 'sale']

    def create(self, validated_data):
        saleData = validated_data.pop('sale')
        userInstance = User.objects.create(**validated_data)
        Sale.objects.create(user=userInstance, **saleData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        sale = Sale.objects.get(user=obj.id)
        return{
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            sale: {
                'id': sale.id,
                'date': sale.date
            }
        }
