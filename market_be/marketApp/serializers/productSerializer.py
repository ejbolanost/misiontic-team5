from rest_framework import serializers
from marketApp.models.product import Product
from marketApp.models.provider import Provider
from marketApp.models.sale import Sale
from marketApp.serializers.saleSerializer import SaleSerializer


class ProductSerializer(serializers.ModelSerializer):
    sale = SaleSerializer()
    provider = ProviderSerializer()

    class Meta:
        model = Product
        fields = ['id', 'name', 'date_expiry',
                  'serial_code', 'provider', 'sale']

    def create(self, validated_data):
        saleData = validated_data.pop('sale')
        productInstance = Product.objects.create(**validated_data)
        Sale.objects.create(product=productInstance, **saleData)
        return productInstance

    def to_representation(self, obj):
        product = Product.objects.get(id=obj.id)
        sale = Sale.objects.get(product=obj.id)
        provider = Provider.objects.get(id=obj.provider)
        return{
            'id': product.id,
            'name': product.name,
            'date_expiry': product.date_expiry,
            'serial_code': product.serial_code,
            provider: {
                'id': provider.id,
                'name': provider.name,
                'phone_number': provider.phone_number,
                'company': provider.company
            },
            sale: {
                'id': sale.id,
                'date': sale.date
            }
        }
