from rest_framework import serializers
from marketApp.models.provider import Provider
from marketApp.models.product import Product
from marketApp.serializers.productSerializer import ProductSerializer


class ProviderSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = Provider
        fields = ['id', 'name', 'phone_number', 'company', 'product']

    def create(self, validated_data):
        productData = validated_data.pop('product')
        providerInstance = Provider.objects.create(**validated_data)
        Product.objects.create(provider=providerInstance, **productData)
        return providerInstance

    def to_representation(self, obj):
        provider = Provider.objects.get(id=obj.id)
        product = Product.objects.get(provider=obj.id)
        return{
            'id': provider.id,
            'name': provider.name,
            'phone_number': provider.phone_number,
            'company': provider.company,
            product: {
                'id': product.id,
                'name': product.name,
                'date_expiry': product.date_expiry,
                'serial_code': product.serial_code
            }
        }
