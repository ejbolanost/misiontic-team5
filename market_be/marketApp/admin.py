from django.contrib import admin
from .models.product import Product
from .models.provider import Provider
from .models.user import User
from .models.sale import Sale

admin.site.register(Product)
admin.site.register(Provider)
admin.site.register(User)
admin.site.register(Sale)

# Register your models here.
