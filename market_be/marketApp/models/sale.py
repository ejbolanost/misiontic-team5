from django.db import models
from .product import Product
from .user import User


class Sale(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True)
    product = models.ForeignKey(
        Product, related_name='sale', on_delete=models.CASCADE)
    user = models.ForeignKey(
        User, related_name='sale', on_delete=models.CASCADE)
