from django.db import models


class Provider(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=60)
    phone_number = models.CharField(max_length=10)
    company = models.CharField(max_length=60)
