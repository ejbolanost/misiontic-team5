from django.db import models
from .provider import Provider


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Nombre del producto', max_length=60)
    date_expiry = models.DateTimeField('Fecha de caducidad')
    serial_code = models.CharField('Codigo serial', max_length=60)
    provider = models.ForeignKey(
        Provider, related_name='product', on_delete=models.CASCADE)
