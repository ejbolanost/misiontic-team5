from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'd8g4hutd2s0jn0',
        'USER': 'jitbgqeqcvnrpr',
        'PASSWORD': '25513fb543590ccd74c284e442d9b8698d4c062a7411035794cfe41c947106c6',
        'HOST': 'ec2-100-24-169-249.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'
